# ScoreBoard MVS Web-Service
### STARTING POINT: Spring Boot version 2.1.6 application from https://start.spring.io/ as a maven project.
### IDE USED: Eclipse
### MATURITY-LEVEL: Executable prototype with low test coverage.

## HOW TO RUN IT:

### From eclipse:
de.freenet.andrschmid.bowling.scoreboard.ScoreboardApplication >> select & right click >> Run As >> Java Application
### From command line:
1. Change to the directory that contains the pom.xml:
- cd scoreboard/
2. Clean, test and package from command line:
- mvn clean
- mvn test
- mvn package
3. Start from command line:
- java -jar ./target/scoreboard-0.0.5.jar

4. Now, browse to "http://localhost:8081/"
...play around. ...

5. Shut down from command line:
- sh ./curl/shutdown.sh

## DESCRIPTON

#### BUILD SYSTEM:
Apache Maven 3.0.5

#### SYSTEM ARCHITECTURE:
Spring Boot MVC

#### SOFTWARE ARCHITECTURE:
GoF Strategy Pattern:
There is a collection of strategies for keeping score.
Each ScoreBoardFrame is parametrized with a suitable strategy.
On score calculation (execution of 'void calculate()') the ScoreCalculator selects the proper strategy from the collection for the ScoreBoardFrame to apply it.

#### FRONT END MANUALLY TESTED USING:
Chromium Version 81.0.4044.122 (64-bit) and
Firefox Version 78.7.0esr (64-bit)
under
LINUX (CentOS 7)
