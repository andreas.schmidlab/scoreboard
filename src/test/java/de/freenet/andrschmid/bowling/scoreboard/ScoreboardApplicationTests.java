package de.freenet.andrschmid.bowling.scoreboard;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;

import de.freenet.andrschmid.bowling.scoreboard.config.AppConfig;
import de.freenet.andrschmid.bowling.scoreboard.controller.ScoreboardController;
import de.freenet.andrschmid.bowling.scoreboard.services.ScoreCalculator;

@RunWith(SpringRunner.class)
@WebFluxTest(controllers = ScoreboardController.class)
@ContextConfiguration(classes = { ScoreboardApplication.class, AppConfig.class, ScoreCalculator.class })
@Import({ ThymeleafAutoConfiguration.class })
public class ScoreboardApplicationTests {

	@Autowired
	WebTestClient webClient;

	@Ignore //TODO: Analyze & fix
	@Test
	public void test() {
		EntityExchangeResult<String> result = webClient.get().uri("/").exchange().expectStatus().isOk()
				.expectBody(String.class).returnResult();
		assertThat(result.getResponseBody()).contains("<span >Bowling Scoreboard</span>");
		MultiValueMap<String, String> formData10 = new LinkedMultiValueMap<>(1);
		formData10.add("nbrOfPinsDown", "10");
		webClient.post().uri("/pinsdown").contentType(MediaType.APPLICATION_FORM_URLENCODED).body(BodyInserters.fromFormData(formData10)).exchange().expectStatus().is3xxRedirection();
		MultiValueMap<String, String> formData01 = new LinkedMultiValueMap<>(1);
		formData01.add("nbrOfPinsDown", "1");
		webClient.post().uri("/pinsdown").contentType(MediaType.APPLICATION_FORM_URLENCODED).body(BodyInserters.fromFormData(formData01)).exchange().expectStatus().is3xxRedirection();
		webClient.post().uri("/pinsdown").contentType(MediaType.APPLICATION_FORM_URLENCODED).body(BodyInserters.fromFormData(formData01)).exchange().expectStatus().is3xxRedirection();
		result = webClient.get().uri("/").exchange().expectStatus().isOk()
				.expectBody(String.class).returnResult();
		assertThat(result.getResponseBody()).contains("<td class=\"myTableCell\" >14</td></td>").contains("<th colspan=\"2\" class=\"tableSubCellScore\">14</th>");
	}
}
