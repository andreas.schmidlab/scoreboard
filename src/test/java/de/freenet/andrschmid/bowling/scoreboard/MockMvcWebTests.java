package de.freenet.andrschmid.bowling.scoreboard;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.HashMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import de.freenet.andrschmid.bowling.scoreboard.config.AppConfig;
import de.freenet.andrschmid.bowling.scoreboard.controller.ScoreboardController;
import de.freenet.andrschmid.bowling.scoreboard.services.ScoreCalculator;
import de.freenet.andrschmid.bowling.scoreboard.strategies.BasicStrategy;
import de.freenet.andrschmid.bowling.scoreboard.strategies.SpareStrategy;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ScoreboardController.class, AppConfig.class, ScoreCalculator.class })
@WebAppConfiguration
public class MockMvcWebTests {

	
	@Autowired
	WebApplicationContext webContext;
	
	@Autowired
	ScoreCalculator scoreCalculator;
	
	@Autowired
	ScoreCalculator scoreCalculator02;
	
	private MockMvc mockMvc;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(webContext).build();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void homePage() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isOk()).andExpect(view().name("scoreboard"))
				.andExpect(model().attributeExists("appName")).andExpect(model().attributeExists("frames"))
				.andExpect(model().attribute("frames", instanceOf(HashMap.class)));
	}
	
	@Test
	public void checkWiring_PUBLIC() {
		assertThat(scoreCalculator).isNotNull();
		//ScoreCalculator scoreCalculator02 = (ScoreCalculator) webContext.getBean("scoreCalculator");
		// really a singleton? 
		assertThat(scoreCalculator).isEqualTo(scoreCalculator02);
		 //webContext.getBean(BasicStrategy.class); -> NoUniqueBeanDefinitionException: No qualifying bean of type 'BasicStrategy' available
		// NOT UNIQUE because wiring with BasicStrategy AND all of its subclasses possible.
		BasicStrategy basicStrategy = (BasicStrategy) webContext.getBean("basicStrategy");
		assertThat(basicStrategy).isNotNull();
		SpareStrategy spareStrategy = webContext.getBean(SpareStrategy.class);
		assertThat(spareStrategy).isNotNull();
	}
	
	@Test
	public void postPinsDown() throws Exception {
		//ScoreCalculator scoreCalculator = (ScoreCalculator) webContext.getBean("scoreCalculator");
		mockMvc.perform(post("/pinsdown").contentType(APPLICATION_FORM_URLENCODED).param("nbrOfPinsDown", "10"))
				.andExpect(status().is3xxRedirection()).andExpect(header().string("Location", "/"));
		assertThat(scoreCalculator.getCurrTotal()).isEqualTo(0);
		mockMvc.perform(post("/pinsdown").contentType(APPLICATION_FORM_URLENCODED).param("nbrOfPinsDown", "10"))
				.andExpect(status().is3xxRedirection()).andExpect(header().string("Location", "/"));
		assertThat(scoreCalculator.getCurrTotal()).isEqualTo(0);
		mockMvc.perform(post("/pinsdown").contentType(APPLICATION_FORM_URLENCODED).param("nbrOfPinsDown", "10"))
				.andExpect(status().is3xxRedirection()).andExpect(header().string("Location", "/"));
		assertThat(scoreCalculator.getCurrTotal()).isEqualTo(30);
	}

}
