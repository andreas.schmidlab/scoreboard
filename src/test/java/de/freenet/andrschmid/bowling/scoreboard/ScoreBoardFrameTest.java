package de.freenet.andrschmid.bowling.scoreboard;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.freenet.andrschmid.bowling.scoreboard.expceptions.NumberOfRollsExceeded;
import de.freenet.andrschmid.bowling.scoreboard.model.ScoreBoardFrame;

public class ScoreBoardFrameTest {
	
	ScoreBoardFrame scoreBoardFrame;

	@Before
	public void setUp() throws Exception {
		this.scoreBoardFrame = new ScoreBoardFrame(1);
	}

	@After
	public void tearDown() throws Exception {
		this.scoreBoardFrame = null;
	}

	@Test
	public void testSumOfPinsKnockedDown() {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1,2,3));
		assertThat(scoreBoardFrame.sumOfPinsKnockedDown()).isEqualTo(6);
	}
	
	@Test
	public void testCalcMaxPossible_MaxValPositive() {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1,2,3));
		assertThat(scoreBoardFrame.calcMaxPossible()).isEqualTo(4);
	}
	
	@Test
	public void testCalcMaxPossible_MaxValNegative() {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1,2,3,5));
		assertThat(scoreBoardFrame.calcMaxPossible()).isEqualTo(-1);
	}
	
	@Test
	public void testCalcMaxPossible_MaxValZero() {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1,2,3,4));
		assertThat(scoreBoardFrame.calcMaxPossible()).isEqualTo(0);
	}
	
	@Test
	public void testIsStrikeRolled_YES() {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(10));
		assertThat(scoreBoardFrame.isStrikeRolled()).isEqualTo(true);
	}
	
	@Test
	public void testIsStrikeRolled_NO_TOO_FEW_PINS() {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(9));
		assertThat(scoreBoardFrame.isStrikeRolled()).isEqualTo(false);
	}

	@Test
	public void testIsStrikeRolled_NO_TOO_MANY_ROLLS() {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1,9));
		assertThat(scoreBoardFrame.isStrikeRolled()).isEqualTo(false);
	}

	@Test
	public void testIsSpareRolled_YES() throws NumberOfRollsExceeded {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1,9));
		assertThat(scoreBoardFrame.isSpareRolled()).isEqualTo(true);
	}
	
	@Test
	public void testIsSpareRolled_NO_SUM_BELOW_10() throws NumberOfRollsExceeded {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1,8));
		assertThat(scoreBoardFrame.isSpareRolled()).isEqualTo(false);
	}
	
	@Test
	public void testIsSpareRolled_NO_NOT_ENOUGH_ROLLS() throws NumberOfRollsExceeded {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1));
		assertThat(scoreBoardFrame.isSpareRolled()).isEqualTo(false);
	}

	@Test(expected=NumberOfRollsExceeded.class)
	public void testIs2ndRollDone_TOO_MANY_ROLLS() throws NumberOfRollsExceeded {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1,2,3,4));
		scoreBoardFrame.is2ndRollDone();
	}
	
	@Test
	public void testIs2ndRollDone_YES() throws NumberOfRollsExceeded {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1,2));
		assertThat(scoreBoardFrame.is2ndRollDone()).isEqualTo(true);
	}
	
	@Test
	public void testIs2ndRollDone_NO() throws NumberOfRollsExceeded {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1));
		assertThat(scoreBoardFrame.is2ndRollDone()).isEqualTo(false);
	}

	@Test
	public void testIsReadyToAdvance_YES_2_ROLLS_DONE() throws NumberOfRollsExceeded {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1,2));
		assertThat(scoreBoardFrame.isReadyToAdvance()).isEqualTo(true);
	}
	
	@Test
	public void testIsReadyToAdvance_YES_STRIKE() throws NumberOfRollsExceeded {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(10));
		assertThat(scoreBoardFrame.isReadyToAdvance()).isEqualTo(true);
	}
	
	@Test
	public void testIsReadyToAdvance_YES_SPARE() throws NumberOfRollsExceeded {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1,9));
		assertThat(scoreBoardFrame.isReadyToAdvance()).isEqualTo(true);
	}
	
	@Test
	public void testIsReadyToAdvance_NO() throws NumberOfRollsExceeded {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1));
		assertThat(scoreBoardFrame.isReadyToAdvance()).isEqualTo(false);
	}
	
	@Test
	public void testIsReadyToAdvance_NO_NONE_EXISTING_PINS_KNOCKED_DOWN() throws NumberOfRollsExceeded {
		scoreBoardFrame.setPinsPerRoll(Arrays.asList(1,10));
		assertThat(scoreBoardFrame.isReadyToAdvance()).isEqualTo(false);
	}

}
