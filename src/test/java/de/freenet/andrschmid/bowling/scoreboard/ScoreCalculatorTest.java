package de.freenet.andrschmid.bowling.scoreboard;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.freenet.andrschmid.bowling.scoreboard.config.AppConfig;
import de.freenet.andrschmid.bowling.scoreboard.expceptions.NumberOfRollsExceeded;
import de.freenet.andrschmid.bowling.scoreboard.services.ScoreCalculator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class, ScoreCalculator.class })
public class ScoreCalculatorTest {
	
	@Autowired
	ScoreCalculator calculator;

	@Before
	public void setUp() throws Exception {
		//this.calculator = new ScoreCalculator();
	}

	@After
	public void tearDown() throws Exception {
		this.calculator.initFrames();
		this.calculator.setNbrOfCurrFrame(1);
		this.calculator.setCurrTotal(0);
		this.calculator.setScoresPending(true);
	}

	@Test
	public void testCalculate_BASIC() {
		this.calculator.getCurrFrame().setPinsPerRoll(Arrays.asList(1,2));
		try {
			this.calculator.calculate(); // calculate and advance -> use getPredecessor()
			assertThat(this.calculator.getCurrFrame().getPredecessor().getScore()).isEqualTo(3);
		} catch (NumberOfRollsExceeded e) {
			fail(e.toString());
		}
	}
	
	@Test
	public void testCalculate_STRIKE() {
		
		try {
			this.calculator.getCurrFrame().setPinsPerRoll(Arrays.asList(1,2));
			this.calculator.calculate(); // calculate and advance
			assertThat(this.calculator.getCurrFrame().getPredecessor().getScore()).isEqualTo(3);
			this.calculator.getCurrFrame().setPinsPerRoll(Arrays.asList(10));
			this.calculator.calculate(); // calculate and advance
			assertThat(this.calculator.getCurrFrame().getPredecessor().getScore()).isNull();
			this.calculator.getCurrFrame().setPinsPerRoll(Arrays.asList(1,2));
			this.calculator.calculate();// calculate and advance
			assertThat(this.calculator.getCurrFrame().getPredecessor().getScore()).isEqualTo(19);
		} catch (NumberOfRollsExceeded e) {
			fail(e.toString());
		}
	}
}
