class Scoreboard {
	static MAX_NBR_OF_PINS = 10;
	/**
	 * @param miss Symbol signifying a "miss"
	 * @param strike Symbol signifying a "strike"
	 * @param spare Symbol signifying a "spare"
	 */
	constructor(miss, strike, spare)
	{
		this.miss = miss;
		this.strike = strike;
		this.spare = spare;
	}
	
	present1stCol(
			isStrikeRolled,
			isFinalFrame,
			pinsFirstRoll, 
			eltId) {
		var rollPresentation = pinsFirstRoll;
	
		if (pinsFirstRoll == 0) {
			rollPresentation = this.miss;
		} else if (pinsFirstRoll == Scoreboard.MAX_NBR_OF_PINS) { // only in effect if final frame
			rollPresentation = this.strike;
		}
	
		if ((pinsFirstRoll != -1) && ((!isStrikeRolled)||(isFinalFrame))) {
			document.getElementById(eltId).innerHTML = rollPresentation;
		}
	}

	present2ndCol(
			isStrikeRolled,
			isSpareRolled,
			isFinalFrame,
			pinsFirstRoll,
			pinsSecondRoll, 
			eltId) {
		var rollPresentation = pinsSecondRoll;
	
		if (pinsSecondRoll == 0) {
			rollPresentation = this.miss;
		} else if ((isStrikeRolled) || ((isFinalFrame)&&(pinsSecondRoll == Scoreboard.MAX_NBR_OF_PINS))) {
			rollPresentation = this.strike;
		} else if (isSpareRolled)  {
			rollPresentation = this.spare;
		}
		
		if ((pinsSecondRoll != -1) || ((isStrikeRolled)&&(!isFinalFrame))) {
			document.getElementById(eltId).innerHTML = rollPresentation;
		}
	}

	present3rdCol(pinsThirdRoll,
			eltId) {
		var rollPresentation = pinsThirdRoll;
	
		if (pinsThirdRoll == 0) {
			rollPresentation = this.miss;
		} else if (pinsThirdRoll == Scoreboard.MAX_NBR_OF_PINS) {
			rollPresentation = this.strike;
		}
		
		if (pinsThirdRoll != -1) {
			document.getElementById(eltId).innerHTML = rollPresentation;
		}
	}
}