package de.freenet.andrschmid.bowling.scoreboard.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import de.freenet.andrschmid.bowling.scoreboard.expceptions.NumberOfRollsExceeded;
import de.freenet.andrschmid.bowling.scoreboard.model.ScoreBoardFrame;
import de.freenet.andrschmid.bowling.scoreboard.services.ScoreCalculator;

/**
 * Controller mapping all get and post requests onto their
 * scoreboard related handler methods.
 * 
 * @author Andreas Schmid
 */
@Controller
public class ScoreboardController {

	
	private static final String GAME_OVER = "GAME OVER";
	private static final String NOT_OK_ONLY_10_PINS_CAN_BE_KNOCKED_DOWN = "NOT OK - Only 10 pins can be knocked down.";
	private static final String INTERNAL_ERROR_TOO_MANY_ROLLS_PER_FRAME = "INTERNAL ERROR - Too many rolls per frame";
	private static final String NOT_OK_NO_ENTRY_MADE = "NOT OK - No entry made.";
	private static final String NOT_OK_ENTRY_MUST_BE_A_NUMBER = "NOT OK - Entry must be a number.";

	private static final String SCOREBOARD = "scoreboard";
	private static final String PINS_DOWN = "/pinsdown";
	private static final String INIT_GAME = "/initGame";
	private static final String HOME = "/";
	private static final String REDIRECT = "redirect:";

	private String entryValidation;

	private ScoreCalculator scoreCalculator;

	@Value("${spring.application.name}")
	private String appName;

	public void setEntryValidation(final String entryValidation) {
		this.entryValidation = entryValidation;
	}

	public ScoreCalculator getScoreCalculator() {
		return scoreCalculator;
	}

	public void setScoreCalculator(final ScoreCalculator scoreCalculator) {
		this.scoreCalculator = scoreCalculator;
	}

	public ScoreboardController(final ScoreCalculator scoreCalculator) {
		super();
		this.scoreCalculator = scoreCalculator;
		init();
	}

	private void init() {
		this.setEntryValidation("");
	}

	@GetMapping(value = HOME)
	public String homePage(final Model model) {
		model.addAttribute("appName", appName);
		model.addAttribute("frames", this.getScoreCalculator().getFrames());
		model.addAttribute("entryValidity", entryValidation);
		model.addAttribute("currTotal", this.getScoreCalculator().getCurrTotal());
		return SCOREBOARD;
	}

	@PostMapping(value = PINS_DOWN)
	public String getPinsKnockedDown(@RequestBody final String nbrOfPinsDown) {
		this.setEntryValidation("");
		if (this.getScoreCalculator().getScoresPending()) {
			Integer pinsDown = null;
			try {
				pinsDown = Integer.parseInt(nbrOfPinsDown.contains("=") ? nbrOfPinsDown.split("=")[1] : "");
			} catch (NumberFormatException e) {
				this.setEntryValidation(NOT_OK_ENTRY_MUST_BE_A_NUMBER);
			} catch (ArrayIndexOutOfBoundsException e) {
				this.setEntryValidation(NOT_OK_NO_ENTRY_MADE);
			}
			final ScoreBoardFrame currFrame = this.getScoreCalculator().getCurrFrame();
			if (pinsDown != null) {
				// if no entry was made (pinsDown's null) we'll try again.

				//Yes, some of the parentheses are useless but improve readability.
				if (((currFrame.sumOfPinsKnockedDown() + pinsDown) <= 10)||(currFrame.getId()==10)) {
					currFrame.getPinsPerRoll().add(pinsDown);

					try {
						this.getScoreCalculator().calculate();
					} catch (NumberOfRollsExceeded e) {
						this.setEntryValidation(INTERNAL_ERROR_TOO_MANY_ROLLS_PER_FRAME);
					}
				} else {
					this.setEntryValidation(NOT_OK_ONLY_10_PINS_CAN_BE_KNOCKED_DOWN);
				}

			}
		} else {
			this.setEntryValidation(GAME_OVER);
		}
		return REDIRECT + HOME;
	}

	@PostMapping(value = INIT_GAME)
	public String initGame() {
		this.init();
		this.getScoreCalculator().initFrames();
		this.getScoreCalculator().setNbrOfCurrFrame(1);
		this.getScoreCalculator().setCurrTotal(0);
		this.getScoreCalculator().setScoresPending(true);
		return REDIRECT + HOME;
	}
}
