package de.freenet.andrschmid.bowling.scoreboard.strategies;

import de.freenet.andrschmid.bowling.scoreboard.expceptions.NumberOfRollsExceeded;
import de.freenet.andrschmid.bowling.scoreboard.model.ScoreBoardFrame;

/**
 * Responsibility:
 * Handle a "spare" in which the remainder of the pins left standing after the first roll 
 * are knocked down on the second roll in a frame.
 * 
 * Role in the context of the Strategy Pattern: 
 * One of the concrete strategies.
 *  
 * @author Andreas Schmid
 *
 */
public class SpareStrategy extends BasicStrategy {
	
	/**
	 *  
	 * For a spare, 
	 * the bowler gets the 10, 
	 * plus the total number of pins knocked down on the next roll only.
	 * 
	 * firstSucceedingRoll: Total number of pins knocked down on the next roll. 
	 */
	@Override
	public void calculate(final ScoreBoardFrame frame) throws NumberOfRollsExceeded {
		Integer firstSucceedingRoll = null;

		final ScoreBoardFrame successor = this.getFrames().get(frame.getId() + 1);

		//Yes, parentheses are useless but improve readability.
		if ((successor != null) && (!successor.getPinsPerRoll().isEmpty())) {
			// There is a successor AND pins were knocked down already.
			firstSucceedingRoll = successor.getPinsFirstRoll();
		}
		if (firstSucceedingRoll != null) {
			super.calculate(frame);
			frame.setScore(frame.getScore() + firstSucceedingRoll);
		}
	}
	
	/**
	 * Provide a textual presentation of the spare strategie's essential
	 * information like its name and the way it calculates its score.
	 * PURPOSE: Ease debugging.
	 */
	@Override
	public String toString() {
		final StringBuilder str = new StringBuilder();
		
		str.append(super.toString())
		.append(" adding succesor's 1st roll to current score;");
		return str.toString();
	}

}