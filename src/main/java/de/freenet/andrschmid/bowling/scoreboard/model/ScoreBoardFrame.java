package de.freenet.andrschmid.bowling.scoreboard.model;

import java.util.ArrayList;
import java.util.List;
import de.freenet.andrschmid.bowling.scoreboard.expceptions.NumberOfRollsExceeded;
import de.freenet.andrschmid.bowling.scoreboard.strategies.ScoreCalculationStrategy;

/**
 * 
 * @author Andreas Schmid
 *
 */
public class ScoreBoardFrame {
	

	/**
	 * 
	 */
	public static final int MAX_NBR_OF_PINS = 10;
	/**
	 * 
	 */
	private Integer id;
	
	/**
	 * Max. nbr of pins to be knocked down on the 2nd roll
	 */
	private Integer maxPossible;
	
	/**
	 * Pins knocked down per roll
	 */
	private List<Integer> pinsPerRoll;
	
	/**
	 * This frame's score
	 */
	private Integer score;
	
	/**
	 * 
	 */
	private ScoreBoardFrame predecessor;

	/**
	 * 
	 */
	private ScoreCalculationStrategy strategy;
	/**
	 * @return
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id
	 */
	public void setId(final Integer id) {
		this.id = id;
	}
	/**
	 * @return
	 */
	public Integer getMaxPossible() {
		return maxPossible;
	}
	
	/**
	 * @param maxPossible
	 */
	public void setMaxPossible(final Integer maxPossible) {
		this.maxPossible = maxPossible;
	}
	/**
	 * @return The list of pins knocked down per roll
	 */
	public List<Integer> getPinsPerRoll() {
		return pinsPerRoll;
	}
	/**
	 * @param pinsPerRoll
	 */
	public void setPinsPerRoll(final List<Integer> pinsPerRoll) {
		this.pinsPerRoll = pinsPerRoll;
	}
	
	/**
	 * @return This frame's score
	 */
	public Integer getScore() {
		return score;
	}
	/**
	 * @param score
	 */
	public void setScore(final Integer score) {
		this.score = score;
	}
	
	/**
	 * @return The current frame's predecessor.
	 */
	public ScoreBoardFrame getPredecessor() {
		return predecessor;
	}
	/**
	 * @param predecessor The preceding frame.
	 */
	public void setPredecessor(final ScoreBoardFrame predecessor) {
		this.predecessor = predecessor;
	}
	
	/**
	 * @return Nbr of pins knocked down by this frames 1st roll.
	 */
	public Integer getPinsFirstRoll() {
		return this.getPinsPerRoll().size() >= 1?this.getPinsPerRoll().get(0):-1;
	}
	
	/**
	 * @return Nbr of pins knocked down by this frames 2nd roll.
	 */
	public Integer getPinsSecondRoll() {
		return this.getPinsPerRoll().size() >= 2?this.getPinsPerRoll().get(1):-1;
	}
	
	/**
	 * @return Nbr of pins knocked down by this frames 3rd roll.
	 */
	public Integer getPinsThirdRoll() {
		return this.getPinsPerRoll().size() >= 3?this.getPinsPerRoll().get(2):-1;
	}
	
	/**
	 * @return All pins knocked down in this frame.
	 */
	public Integer sumOfPinsKnockedDown() {
		return this.getPinsPerRoll().stream().mapToInt(i -> i).sum();
	}
	
	/**
	 * Blueprint of a frame containing its id, the list of pins knocked down per roll 
	 * and its score. 
	 * @param id The id of this frame.
	 */
	public ScoreBoardFrame(final Integer id) {
		super();
		this.setId(id);
		this.setPinsPerRoll(new ArrayList<>());
		this.setScore(null);
	}
	
	/**
	 * @return maximum possible number of pins still to be knocked down.
	 */
	public Integer calcMaxPossible() {
		//Subtract the sum of all pins knocked down from 
		// the max. nbr of pins (using IntStream.sum()) 
		return MAX_NBR_OF_PINS - this.sumOfPinsKnockedDown();
	}
	
	/**
	 * @return true, if strike is rolled - false otherwise
	 */
	public Boolean isStrikeRolled() {
		// STRIKE: 1st roll knocks down all 10 pins -> max. nbr of pins for the 2nd roll is zero.
		return (this.getPinsPerRoll().size() == 1)&& (this.calcMaxPossible() == 0);
	}
	
	/**
	 * @return true, if spare is rolled - false otherwise
	 * @throws NumberOfRollsExceeded
	 */
	public Boolean isSpareRolled() throws NumberOfRollsExceeded {
		// SPARE: Pins knocked down after the 1st and the 2nd roll sum up to 10 (max nbr. of pins)
		return (this.is2ndRollDone()) && ((this.getPinsFirstRoll() + this.getPinsSecondRoll()) == ScoreBoardFrame.MAX_NBR_OF_PINS);		
	}
	
	/**
	 * @return true, if 2nd roll is done - false otherwise
	 * @throws NumberOfRollsExceeded Thrown, if more than 3 rolls were done.
	 */
	public Boolean is2ndRollDone() throws NumberOfRollsExceeded {
		if (this.getPinsPerRoll().size() > 3) {
			throw new NumberOfRollsExceeded("NBR of rolls: " + this.getPinsPerRoll().size());
		}
		return (this.getPinsPerRoll().size() >= 2);
	}
	
	/**
	 * @return true, if all prerequisites for advancing to the next frame are met - false otherweise.
	 * @throws NumberOfRollsExceeded
	 */
	public Boolean isReadyToAdvance() throws NumberOfRollsExceeded {
		return (this.isStrikeRolled() || this.is2ndRollDone())&&(this.calcMaxPossible()>=0);
	}
	/**
	 * @return The strategy this frame is parametrized with.
	 */
	public ScoreCalculationStrategy getStrategy() {
		return strategy;
	}
	/**
	 * @param currStrategy The strategy this frame is to be parametrized with.
	 */
	public void setStrategy(final ScoreCalculationStrategy currStrategy) {
		this.strategy = currStrategy;
	}
	
	/**
	 * Provide a textual presentation of the current frame's essential
	 * information like the strategy it applies, the pins knocked down and 
	 * it current score. "N-Y-S" stands for Not-Yet-Set.
	 * PURPOSE: Ease debugging.
	 */
	@Override
	public String toString() {
		final StringBuilder str = new StringBuilder();
		
		str.append("[ FRM-").append(String.format("%03d", this.getId()))
		.append(" applys ")
		.append(this.getStrategy() == null?"NO strategy, yet":this.getStrategy())
		.append(" pins down (")
		.append(this.getPinsFirstRoll())
		.append("/")
		.append(this.getPinsSecondRoll())
		.append("/")
		.append(this.getPinsThirdRoll())
		.append(") -1 == N-Y-S;") //N-Y-S: Not Yet Set
		.append(" curr. score: ")
		.append(this.getScore()!=null?this.getScore():"N-Y-S")
		.append(" ]");
		return str.toString();
	}
}
