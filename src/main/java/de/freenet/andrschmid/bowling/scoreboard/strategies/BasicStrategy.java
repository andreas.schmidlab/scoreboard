package de.freenet.andrschmid.bowling.scoreboard.strategies;

import java.util.Map;

import de.freenet.andrschmid.bowling.scoreboard.expceptions.NumberOfRollsExceeded;
import de.freenet.andrschmid.bowling.scoreboard.model.ScoreBoardFrame;

/**
 * @author Andreas Schmid
 * 
 * Role in the context of the Strategy Pattern: 
 * One of the concrete strategies.
 */
public class BasicStrategy implements ScoreCalculationStrategy {

	/**
	 * 
	 */
	private Map<Integer, ScoreBoardFrame> frames;

	/**
	 * @return the map of frames
	 */
	public Map<Integer, ScoreBoardFrame> getFrames() {
		return frames;
	}

	/**
	 * @param frames the map of frames
	 */
	public void setFrames(final Map<Integer, ScoreBoardFrame> frames) {
		this.frames = frames;
	}

	/**
	 * Keep score by adding the number of pins knocked down in each frame.
	 * Special bonuses are awarded for strikes and spares. 
	 * Theses bonuses are handled by sub classes.
	 */
	@Override
	public void calculate(final ScoreBoardFrame frame) throws NumberOfRollsExceeded {
		frame.setScore(
				(frame.getPredecessor() != null ? frame.getPredecessor().getScore() : 0) + frame.sumOfPinsKnockedDown());
	}

	/**
	 * Provide a textual presentation of the basic strategie's essential
	 * information which is essentially its name.
	 * PURPOSE: Ease debugging.
	 */
	@Override
	public String toString() {
		final StringBuilder str = new StringBuilder();
	
		str.append(this.getClass().getSimpleName());
		return str.toString();
	}
}
