package de.freenet.andrschmid.bowling.scoreboard.strategies;

import de.freenet.andrschmid.bowling.scoreboard.expceptions.NumberOfRollsExceeded;
import de.freenet.andrschmid.bowling.scoreboard.model.ScoreBoardFrame;

/**
 * 
 * https://en.wikipedia.org/wiki/Strategy_pattern - 
 * Behavioral design pattern that enables selecting an algorithm at runtime. 
 * 
 * Instead of implementing a single algorithm directly, 
 * the ScoreCalculator service receives run-time instructions as to which in a family of algorithms to use.
 * In this context this family is composed of algorithms to handle 
 * a strike,
 * a spare,
 * the final frame and
 * a basic score.
 * 
 * Role in the context of the Strategy Pattern: Strategy Interface
 * 
 * @author Andreas Schmid
 *
 */
public interface ScoreCalculationStrategy {
	/**
	 * @param frame
	 * @throws NumberOfRollsExceeded Thrown, if more than 3 rolls were done.
	 */
	void calculate(ScoreBoardFrame frame) throws NumberOfRollsExceeded;
}