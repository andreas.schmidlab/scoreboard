package de.freenet.andrschmid.bowling.scoreboard;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author Andreas Schmid
 *
 */
@SpringBootApplication
public class ScoreboardApplication {
	/**
	 * 
	 */
	private static ConfigurableApplicationContext context;

	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		context = SpringApplication.run(ScoreboardApplication.class, args);
	}
	
	/**
	 * Restart the application e.g. on receipt of a restart POST request.
	 */
	public static void restart() {
        final ApplicationArguments args = context.getBean(ApplicationArguments.class);
 
        final Thread thread = new Thread(() -> {
            context.close();
            context = SpringApplication.run(ScoreboardApplication.class, args.getSourceArgs());
        });
 
        thread.setDaemon(false);
        thread.start();
    }
}
