package de.freenet.andrschmid.bowling.scoreboard.services;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import de.freenet.andrschmid.bowling.scoreboard.expceptions.NumberOfRollsExceeded;
import de.freenet.andrschmid.bowling.scoreboard.model.ScoreBoardFrame;
import de.freenet.andrschmid.bowling.scoreboard.strategies.BasicStrategy;
import de.freenet.andrschmid.bowling.scoreboard.strategies.FinalStrategy;
import de.freenet.andrschmid.bowling.scoreboard.strategies.SpareStrategy;
import de.freenet.andrschmid.bowling.scoreboard.strategies.StrikeStrategy;


/**
 * 
 * Role in the context of the Strategy Pattern: 
 * The Context referring to the Strategy interface for performing an algorithm.
 * 
 * 
 * @author Andreas Schmid
 * 
 *
 */
@Service
public class ScoreCalculator {

	/**
	 * There mustn't be more the 10 frames 
	 */
	private static final int MAX_NBR_OF_FRAMES = 10;

	/**
	 * The 10 frames a game of bowling consists of.
	 */
	private Map<Integer, ScoreBoardFrame> frames;

	/**
	 * Identifies the current frame.
	 */
	private Integer nbrOfCurrFrame;
	/**
	 * 
	 */
	private Integer currTotal;
	/**
	 * 
	 */
	private Boolean scoresPending;
	
	/**
	 * 
	 */
	private BasicStrategy basicStrat;
	
	/**
	 * 
	 */
	private StrikeStrategy strikeStrat;
	
	/**
	 * 
	 */
	private SpareStrategy spareStrat;
	
	/**
	 * 
	 */
	private FinalStrategy finalStrat;

	/**
	 * @return
	 */
	public Map<Integer, ScoreBoardFrame> getFrames() {
		return frames;
	}

	/**
	 * @param frames
	 */
	@Autowired
	public void setFrames(final Map<Integer, ScoreBoardFrame> frames) {
		this.frames = frames;
	}

	/**
	 * @return
	 */
	public Integer getNbrOfCurrFrame() {
		return nbrOfCurrFrame;
	}

	/**
	 * @param nbrOfCurrFrame
	 */
	public void setNbrOfCurrFrame(final Integer nbrOfCurrFrame) {
		this.nbrOfCurrFrame = nbrOfCurrFrame;
	}

	/**
	 * @return
	 */
	public Integer getCurrTotal() {
		return currTotal;
	}

	/**
	 * @param currTotal
	 */
	public void setCurrTotal(final Integer currTotal) {
		this.currTotal = currTotal;
	}

	/**
	 * @return
	 */
	public ScoreBoardFrame getCurrFrame() {
		return this.getFrames().get(this.getNbrOfCurrFrame());
	}

	/**
	 * @return
	 */
	public Boolean getScoresPending() {
		return scoresPending;
	}

	/**
	 * @param scoresPending
	 */
	public void setScoresPending(final Boolean scoresPending) {
		this.scoresPending = scoresPending;
	}

	/**
	 * @param basicStrat
	 * @param strikeStrat
	 * @param spareStrat
	 * @param finalStrat
	 */
	public ScoreCalculator(
			@Qualifier("basicStrategy")
			BasicStrategy basicStrat, 
			StrikeStrategy strikeStrat, 
			SpareStrategy spareStrat, 
			FinalStrategy finalStrat) {
		super();

		this.basicStrat = basicStrat;
		this.strikeStrat = strikeStrat;
		this.spareStrat = spareStrat;
		this.finalStrat = finalStrat;

		this.setNbrOfCurrFrame(1);
		this.setCurrTotal(0);
		this.setScoresPending(true);
	}

	/**
	 * 
	 */
	public void initFrames() {
		for (final ScoreBoardFrame frame:this.getFrames().values()) {
			frame.setPinsPerRoll(new ArrayList<>());
			frame.setScore(null);
			frame.setStrategy(null);
		}
	}

	/**
	 * Calculates the pending scores.
	 * Acting in the role of the Context calling calculate() on the strategy held, 
	 * by the respective frame.
	 * @throws NumberOfRollsExceeded 
	 */
	private void calculatePendingScores() throws NumberOfRollsExceeded {
		
		for (final ScoreBoardFrame frame : this.getFrames().values()) {
			if ((frame.getScore() == null) && (frame.getStrategy() != null)) {
				frame.getStrategy().calculate(frame);
			}
		}
		// We don't want to report a score as pending that we just fixed.
		// Let's check them all.
		Boolean scoresPending = false;
		for (final ScoreBoardFrame frame : this.getFrames().values()) {
			if (frame.getScore() == null) {
				scoresPending = true;
				break;
			}
		}
		this.setScoresPending(scoresPending);
	}

	/**
	 * Parametrize the current frame with the proper strategy and calculate the scores
	 * @throws NumberOfRollsExceeded
	 */
	public void calculate() throws NumberOfRollsExceeded {
		final ScoreBoardFrame currFrame = this.getCurrFrame();
		if (currFrame.getId() == MAX_NBR_OF_FRAMES) {
			currFrame.setStrategy(finalStrat);
		} else if (currFrame.isStrikeRolled()) {
			strikeStrat.setFrames(this.getFrames());
			currFrame.setStrategy(strikeStrat);
		} else if (currFrame.isSpareRolled()) {
			spareStrat.setFrames(this.getFrames());
			currFrame.setStrategy(spareStrat);
		} else if (currFrame.is2ndRollDone()) {
			currFrame.setStrategy(basicStrat);
		}
		// Calculate scores
		this.calculatePendingScores();

		// Make sure the current total is the biggest of 
		// all scores available (== not null)
		this.setCurrTotal(this.calcCurrTotal());

		if ((this.getNbrOfCurrFrame() < MAX_NBR_OF_FRAMES) && (currFrame.isReadyToAdvance())) {
			// Before we advance we need to be sure we're not in the last frame
			this.setNbrOfCurrFrame(this.getNbrOfCurrFrame() + 1);
		}
	}

	/**
	 * @return
	 */
	private Integer calcCurrTotal() {
		Integer currTotal = 0;
		for (final ScoreBoardFrame currFrame:this.getFrames().values()) {
			final Integer currScore = currFrame.getScore();
			//Yes, parentheses are useless but improve readability.
			if ((currScore != null) && (currScore > currTotal)) {
				currTotal = currScore;
			}
		}
		return currTotal;
	}
}
