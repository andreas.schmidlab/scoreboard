package de.freenet.andrschmid.bowling.scoreboard.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import de.freenet.andrschmid.bowling.scoreboard.ScoreboardApplication;

/**
 * @author Andreas Schmid
 * Only responsibility: 
 * Handle restart the application triggered by an HTTP request.
 */
@Controller
public class RestartController {
     
    /**
     * @return Page to be displayed after restart was triggered
     */
    @PostMapping("/restart")
    public String restart() {
    	ScoreboardApplication.restart();
    	return "restart";
    } 
}
