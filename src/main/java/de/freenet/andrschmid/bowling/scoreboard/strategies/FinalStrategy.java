package de.freenet.andrschmid.bowling.scoreboard.strategies;

import de.freenet.andrschmid.bowling.scoreboard.expceptions.NumberOfRollsExceeded;
import de.freenet.andrschmid.bowling.scoreboard.model.ScoreBoardFrame;

/**
 * Responsibility:
 * Handle the final frame.
 * 
 * A game of bowling consists of 10 frames, 
 * in which a bowler has two chances per frame to knock down all ten pins.
 * If a bowler knocks down all ten pins in those two turns in the 10th and hence the final frame, 
 * an extra bonus chance is awarded.
 * 
 * In other words: 
 * 1) You're in the final frame
 * 2) You get a strike or a spare in the first 2 rolls 
 * CONSEQUENCE: You get another roll.
 * 
 * Role in the context of the Strategy Pattern: 
 * One of the concrete strategies.
 * 
 * @author Andreas Schmid
 *
 */
public class FinalStrategy extends BasicStrategy {

	/**
	 * If a strike or a spare is rolled in the 1st 2 rolls 
	 * an additional roll is granted 
	 */
	@Override
	public void calculate(final ScoreBoardFrame frame) throws NumberOfRollsExceeded {
		if (frame.getPredecessor().getScore() != null) {
			
			final Boolean hasStrike = !frame.getPinsPerRoll().isEmpty() && frame.getPinsPerRoll().contains(ScoreBoardFrame.MAX_NBR_OF_PINS);
			final Boolean isSpare = frame.isSpareRolled();
			// You get a strike or a spare in the first 2 rolls then you get another roll.
			// nbrOfRollsGranted is set accordingly.
			final Integer nbrOfRollsGranted = (hasStrike || isSpare)?3:2;//Yes, parentheses are useless but improve readability.
			if (frame.getPinsPerRoll().size() ==nbrOfRollsGranted) {
				super.calculate(frame);
			}
		}
	}
	
	
	/**
	 * Provide a textual presentation of the final strategie's essential
	 * information like its name, the way the final frame is handled and
	 * the score is calculated.
	 * PURPOSE: Ease debugging.
	 */
	@Override
	public String toString() {
		final StringBuilder str = new StringBuilder();
		
		str.append(super.toString())
		.append(" adding the score of another roll in case of a strike or a spare in the first 2 rolls;");
		return str.toString();
	}
}
