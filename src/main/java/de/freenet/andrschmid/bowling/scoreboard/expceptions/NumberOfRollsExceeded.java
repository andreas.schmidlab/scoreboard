package de.freenet.andrschmid.bowling.scoreboard.expceptions;

public class NumberOfRollsExceeded extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NumberOfRollsExceeded(final String message) {
		super(message);
	}
}
