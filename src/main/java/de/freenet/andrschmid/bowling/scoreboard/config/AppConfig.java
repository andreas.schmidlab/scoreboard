package de.freenet.andrschmid.bowling.scoreboard.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import de.freenet.andrschmid.bowling.scoreboard.model.ScoreBoardFrame;
import de.freenet.andrschmid.bowling.scoreboard.strategies.BasicStrategy;
import de.freenet.andrschmid.bowling.scoreboard.strategies.FinalStrategy;
import de.freenet.andrschmid.bowling.scoreboard.strategies.SpareStrategy;
import de.freenet.andrschmid.bowling.scoreboard.strategies.StrikeStrategy;

/**
 * @author Andreas Schmid
 *
 */
@Configuration
public class AppConfig {

	/**
	 * 
	 */
	private static final int MAX_NBR_OF_FRAMES = 10;

    /**
     * @return The instance of the basic strategy
     */
    @Bean
    public BasicStrategy basicStrategy() {
        return new BasicStrategy();
    }
    
    /**
     * @return The instance of the strike strategy
     */
    @Bean
    protected StrikeStrategy strikeStrategy() {
    	return new StrikeStrategy();
    }
    
    /**
     * @return The instance of the spare strategy
     */
    @Bean
    protected SpareStrategy spareStrategy() {
    	return new SpareStrategy();
    }
    
    /**
     * @return The instance of the final strategy
     */
    @Bean
    protected FinalStrategy finalStrategy() {
    	return new FinalStrategy();
    }
    
    /**
     * @param id The id of the new frame.
     * @return AN (scope == prototype) instance of the score board frame 
     * to be parameterized with its id
     */
    @Bean
    @Scope("prototype")
    protected ScoreBoardFrame scoreBoardFrame(final Integer id) {
    	return new ScoreBoardFrame(id);
    }
    
    /**
     * @return The id to score board frame mapping
     */
    @Bean
    public Map<Integer, ScoreBoardFrame> frameMap() {
    	final Map<Integer, ScoreBoardFrame> frames = new HashMap<>();
		for (int i = 1; i <= MAX_NBR_OF_FRAMES; i++) {
			final ScoreBoardFrame frame = scoreBoardFrame(i);
			frames.put(i, frame);
			if (i > 1) {
				frame.setPredecessor(frames.get(i - 1));
			}
		}
    	return frames;
    }
}

