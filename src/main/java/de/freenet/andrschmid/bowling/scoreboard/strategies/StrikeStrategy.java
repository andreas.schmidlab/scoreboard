package de.freenet.andrschmid.bowling.scoreboard.strategies;

import de.freenet.andrschmid.bowling.scoreboard.expceptions.NumberOfRollsExceeded;
import de.freenet.andrschmid.bowling.scoreboard.model.ScoreBoardFrame;

/**
 * Responsibility:
 * Handle a "strike" in which all ten pins have been knocked down by the first roll 
 * of the ball in a frame.
 * 
 * @author Andreas Schmid
 *
 */
public class StrikeStrategy extends BasicStrategy {

	/**
	 *  
	 * For a strike, 
	 * the bowler is awarded the score of 10 (for knocking down all ten pins), 
	 * plus he gets to add the total of his next two rolls to that frame.
	 * 
	 * firstSucceedingRoll + secondSucceedingRoll: The total of his next two rolls.
	 */
	@Override
	public void calculate(final ScoreBoardFrame frame) throws NumberOfRollsExceeded {

		Integer firstSucceedingRoll = null;
		Integer secondSucceedingRoll = null;
		final ScoreBoardFrame successor = this.getFrames().get(frame.getId() + 1);

		//Yes, parentheses are useless but improve readability.
		if ((successor!=null)&&(!successor.getPinsPerRoll().isEmpty())) {
			// There is a successor AND pins have been knocked down already.
			// Let's get the pins knocked down by the successor's 1st roll.
			firstSucceedingRoll = successor.getPinsFirstRoll();
			if (successor.getPinsPerRoll().size() == 2) {
				// 2 rolls have been made. (2nd roll wasn't another strike)
				// Let's get the pins knocked down by the successor's 2nd roll
				secondSucceedingRoll = successor.getPinsSecondRoll();
			} else {
				//2nd roll WAS another strike
				final ScoreBoardFrame secondSuccessor = this.getFrames().get(successor.getId() + 1);
				if ((secondSuccessor != null) && (!secondSuccessor.getPinsPerRoll().isEmpty())) {
					// The successor has a succeeding frame 
					// AND pins have already been knocked down in the succeeding frame
					secondSucceedingRoll = secondSuccessor.getPinsFirstRoll();
				}
			}
		}
		if ((firstSucceedingRoll != null) && (secondSucceedingRoll != null)) {
			super.calculate(frame);
			frame.setScore(frame.getScore() + firstSucceedingRoll + secondSucceedingRoll);
		}
	}
	
	/**
	 * Provide a textual presentation of the strike strategie's essential
	 * information like its name and the way the score is calculated.
	 * PURPOSE: Ease debugging.
	 */
	@Override
	public String toString() {
		final StringBuilder str = new StringBuilder();
		
		str.append(super.toString())
		.append(" adding the total of the next two rolls;");
		return str.toString();
	}

}
